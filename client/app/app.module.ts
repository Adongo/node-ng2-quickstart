//Defines AppModule, the root module that tells Angular how to assemble the application.
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import {HomePageComponent} from './components/homepage/hompage.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent , HomePageComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
